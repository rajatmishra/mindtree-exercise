(function () {
  'use strict';

  angular
    .module('components', [
      'components.home',
      'components.about'
    ]);

})();
