(function () {
  'use strict';

  angular
    .module('components.about')
    .controller('AboutController', AboutController);


  function AboutController (RestConnectionService, $scope) {
    var vm = {
      $onInit    : $onInit,
      $postLink  : $postLink,
      $onDestroy : $onDestroy,

      title      : 'About'   // just for unit-testing
    };

    angular.extend(this, vm);


    function $onInit () {
      vm.location = 'admr.nl.amsterdam';
      $scope.garage = '';
      $scope.currentPage = 1;
      $scope.numPerPage = 5;
    }
    function $postLink () {
      var data = RestConnectionService.get({in: vm.location, per_page:'100'});

      data.$promise.then(function (response) {
        $scope.garage = response.features;

        console.log(vm.garage);
        $scope.$applyAsync();
      });

      $scope.onClick = function (item) {
        console.log(item);
      };

/*    var GoogleLayer = new L.Google('ROADMAP');
    Map = new L.Map('map', {center: new L.LatLng(52.37, 4.89), zoom: 11, layers: GoogleLayer, draggable: false});
*/    }
    function $onDestroy () {}
  }

})();
