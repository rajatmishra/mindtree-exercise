(function () {
  'use strict';

  angular.module('common', [
    'ui.router',
    'ui.bootstrap',
    'ngResource'
  ]);

})();
