(function () {
  'use strict';

  angular
    .module('common')
    .factory('RestConnectionService', RestConnectionService);

  function RestConnectionService ($resource) {
  	//return $resource('http://jsonplaceholder.typicode.com/users/:user', {user: "@user"});
  //	return $resource('http://api.citysdk.waag.org/layers/:layer', {layer: "@layer", object1: "@per_page"});
  	return $resource('http://api.citysdk.waag.org/layers/parking.garage/objects');
  }

})();
