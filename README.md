# Mindtree Exercise v0.1.0


## Requirements

- NodeJS (v0.12.0 or later)
- Bower

## Installation

**1.** Install gulp globally, which is giving access to gulp's CLI:

```sh
$ npm install gulp -g
```

**2.** Install gulp locally to the project with other required plugins:

```sh
$ npm install
```

**3.** Install required libraries:

```sh
$ bower install
```

## Usage

To build the application simply type:

```sh
$ gulp build
```

Deploy the production version by running `gulp compile`, or simpler:

```sh
$ gulp
```

For development purposes, run `watch` task to build and start local web server with Browsersync:

```sh
$ gulp watch
```

Please note that by default `watch` process incorporates Karma unit testing along with opening new browser window for local web server. You can disable these features by running `watch` task with additional parameters put in command line. Use `--notest` to disable Karma, or `--nobrowser` to disable opening new browser window. Both can be combined in following way:

```sh
$ gulp watch --notest --nobrowser
```

```
├── app/
|   ├── common/
|	|   ├── factory
| 	|   |   ├── RestCoonectionService.js
|   |   ├── nav/
|   |   |   ├── nav.module.js
|   |   |   ├── nav.component.js
|   |   |   ├── nav.service.js
|   |   |   ├── nav.spec.js
|   |   |   ├── nav.html
|   |   |   └── nav.scss
|   |   ├── footer/
|   |   |   ├── footer.module.js
|   |   |   ├── footer.component.js
|   |   |   ├── footer.service.js
|   |   |   ├── footer.spec.js
|   |   |   ├── footer.html
|   |   |   └── footer.scss
|   |   └── common.module.js
│   ├── components/
|   |   ├── home/
|   |   |   ├── home.module.js
|   |   |   ├── home.component.js
|   |   |   ├── home.directive.js
|   |   |   ├── home.service.js
|   |   |   ├── home.spec.js
|   |   |   ├── home.html
|   |   |   └── home.scss
|   |   ├── about/
|   |   |   ├── about-contact/
|   |   |   |   ├── about-contact.module.js
|   |   |   |   ├── about-contact.component.js
|   |   |   |   ├── about-contact.service.js
|   |   |   |   ├── about-contact.spec.js
|   |   |   |   ├── about-contact.html
|   |   |   |   └── about-contact.scss
|   |   |   ├── about.module.js
|   |   |   ├── about.component.js
|   |   |   ├── about.directive.js
|   |   |   ├── about.service.js
|   |   |   ├── about.spec.js
|   |   |   ├── about.html
|   |   |   └── about.scss
|   |   └── components.module.js
|   ├── root.module.js
|   ├── root.component.js
|   └── root.spec.js
└── index.html
```
